# Documentación del Código

## Clase Archivo

La clase `Archivo` representa un archivo con los siguientes atributos:
- `id`: El identificador del archivo.
- `extension`: La extensión del archivo (por ejemplo, "pdf", "png").
- `archivo`: Un arreglo de bytes que almacena los datos del archivo.

La clase proporciona constructores y métodos para acceder y modificar estos atributos.

## Clase ArchivoDAO

La clase `ArchivoDAO` implementa la interfaz `DAO` y se encarga de la interacción con la base de datos. Aquí se destacan los métodos más importantes:

- `getById(int id)`: Recupera un archivo específico desde la base de datos por su `id`. Se utiliza una instancia de `ConexionMySQL` para establecer una conexión con la base de datos. Luego, se recupera el archivo en formato de bytes desde la base de datos y se crea un archivo temporal en el sistema. Finalmente, se abre el archivo temporal con la aplicación predeterminada.

- `add(Archivo archivo)`: Agrega un nuevo archivo a la base de datos. Se utiliza una sentencia SQL para insertar el archivo en la base de datos.

## Interfaz DAO

La interfaz `DAO` define los métodos que deben ser implementados por las clases DAO. Los métodos incluyen `getAll()`, `getById(int id)`, `update(T t)`, `delete(T t)`, y `add(T t)`.


## Clase FileUI

La clase `FileUI` es una interfaz de usuario simple para ingresar el ID de un archivo y abrirlo. Utiliza una instancia de `ArchivoDAO` para realizar esta operación.

## Clase FileUploader2

La clase `FileUploader2` implementa una interfaz gráfica para cargar archivos en el sistema. Algunos puntos clave son:

- Permite la selección de archivos y los almacena en la base de datos utilizando `ArchivoDAO`.

- Utiliza un arreglo de botones "Seleccionar" para cargar hasta tres archivos diferentes.

- Cada vez que se selecciona un archivo, se guarda en la base de datos y se crea un archivo temporal en el sistema. Luego, se abre el archivo temporal con la aplicación predeterminada.

- La clase es iniciada a través del método `main` y se crea una instancia de `ArchivoDAO` para interactuar con la base de datos.

# Cómo Subir Archivos en Java

El codigo se encarga de subir archivos en Java y consta de las siguientes etapas:

1. **Selector de Archivos**: Comienza por crear un cuadro de diálogo de selección de archivos (`JFileChooser`), que permite a los usuarios elegir un archivo de su sistema. Se configura un filtro para restringir los tipos de archivos permitidos, en este caso, PDF y PNG.

2. **Lectura del Archivo**: Una vez que el usuario selecciona un archivo, se crea un `FileInputStream`. Esta clase se especializa en la lectura de bytes desde un archivo. El archivo seleccionado se abre y se inicia el proceso de lectura.

3. **Escritura en ByteArrayOutputStream**: Conforme se leen los datos del archivo, se escriben en un `ByteArrayOutputStream`. Esta clase funciona como un "depósito" interno que almacena los datos en forma de un arreglo de bytes.

4. **Conversión a un Array de Bytes**: Una vez que se ha leído todo el contenido del archivo y se ha escrito en el `ByteArrayOutputStream`, se puede recuperar esa información en forma de un array de bytes utilizando el método `toByteArray()`.

5. **Almacenamiento en la Base de Datos**: Por último, se crea una nueva instancia de la clase `Archivo` que contiene los datos del archivo y su extensión. Luego, se añade esta instancia a la base de datos utilizando el DAO correspondiente.

# Fuentes
- [java.io.InputStream](https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html)
- [javax.swing.JFileChooser](https://docs.oracle.com/javase/8/docs/api/javax/swing/JFileChooser.html)
