package ExamplePOC;


public class Archivo {

  private int id;
  private String extension;
  private byte[] archivo;

  public Archivo(byte[] archivo, String extension) {
    this.archivo = archivo;
    this.extension = extension;
  }

  public Archivo(int id, byte[] archivo) {
    this.id = id;
    this.archivo = archivo;
  }

  public String getExtension() {
    return extension;
  }

  public void setExtension(String extension) {
    this.extension = extension;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public byte[] getArchivo() {
    return archivo;
  }

  public void setArchivo(byte[] archivo) {
    this.archivo = archivo;
  }
}
