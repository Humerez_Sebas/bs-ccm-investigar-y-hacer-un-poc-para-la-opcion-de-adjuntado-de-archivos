package ExamplePOC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionMySQL {

  private Connection con;


  public ConexionMySQL() {
    try {

      Class.forName("com.mysql.cj.jdbc.Driver");
      String url = "jdbc:mysql://localhost:3306/poc";

      con = DriverManager.getConnection(url, "root", "AdminRoot123");

    } catch (Exception e) {
      System.err.println("Error: " + e);
    }
  }

  public static ConexionMySQL getInstance() {
    return new ConexionMySQL();
  }

  public Connection getConnection() {
    return con;
  }

  public String getDatabaseName() {
    String dbName = null;
    try {
      dbName = con.getCatalog();
    } catch (SQLException e) {
      System.err.println("Error: " + e);
    }
    return dbName;
  }

  public void closeConnection() throws SQLException {
    try {
      con.close();
    } catch (Exception ex) {
    }
  }
}
