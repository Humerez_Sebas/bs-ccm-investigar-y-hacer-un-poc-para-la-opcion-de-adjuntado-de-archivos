package ExamplePOC;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface DAO<T> {
  List<T> getAll();
  T getById(int id) throws SQLException, IOException;
  void update(T t);
  void delete(T t);
  void add(T t);
}
