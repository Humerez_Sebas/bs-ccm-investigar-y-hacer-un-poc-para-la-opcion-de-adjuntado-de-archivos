package ExamplePOC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FileUI extends JFrame {
  private DAO<Archivo> dao;
  private JTextField idField;

  public FileUI() {
    dao = new ArchivoDAO();

    this.setLayout(new FlowLayout());

    JLabel idLabel = new JLabel("Introduce el ID del archivo:");
    this.add(idLabel);

    idField = new JTextField(10);
    this.add(idField);

    JButton openButton = new JButton("Abrir archivo");
    openButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        int id = Integer.parseInt(idField.getText());
        try {
          dao.getById(id);
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    });
    this.add(openButton);

    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.pack();
    this.setVisible(true);
  }

  public static void main(String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        new FileUI();
      }
    });
  }
}
