package ExamplePOC;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.swing.filechooser.FileNameExtensionFilter;

public class FileUploader2 extends JFrame {

  private JButton[] buttonBrowse;
  private File[] selectedFile;
  private int numButtons = 3;
  private Archivo archivo;
  private DAO<Archivo> dao;

  public FileUploader2(DAO<Archivo> dao) {
    this.dao = dao;
    createUIComponents();

    try {
      UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
    } catch (Exception ignored) {
    }

    for (int i = 0; i < numButtons; i++) {
      final int index = i;
      buttonBrowse[i].addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          JFileChooser fileChooser = new JFileChooser();
          FileNameExtensionFilter filter = new FileNameExtensionFilter("PDF files", "PNG files",
              "pdf", "png");
          fileChooser.setFileFilter(filter);
          int returnValue = fileChooser.showOpenDialog(null);
          if (returnValue == JFileChooser.APPROVE_OPTION) {
            selectedFile[index] = fileChooser.getSelectedFile();

            String extension = "";
            String fileName = selectedFile[index].getName();
            int i = fileName.lastIndexOf('.');
            if (i > 0) {
              extension = fileName.substring(i + 1);
            }

            try (InputStream inputStream = new FileInputStream(selectedFile[index]);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {

              byte[] buffer = new byte[1024];
              int bytesRead;
              while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
              }

              byte[] fileBytes = outputStream.toByteArray();
              archivo = new Archivo(fileBytes, extension);
              dao.add(archivo);

            } catch (Exception ex) {
              ex.printStackTrace();
            }
          }
        }
      });
    }
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.pack();
    this.setSize(800, 600);
    this.setVisible(true);
  }


  private void createUIComponents() {
    this.setLayout(new GridLayout(numButtons, 1));

    buttonBrowse = new JButton[numButtons];
    selectedFile = new File[numButtons];

    for (int i = 0; i < numButtons; i++) {
      JPanel panel = new JPanel();
      panel.setLayout(new FlowLayout());

      buttonBrowse[i] = new JButton("Seleccionar");
      buttonBrowse[i].setBackground(Color.CYAN);
      panel.add(buttonBrowse[i]);

      this.add(panel);
    }
  }

  public static void main(String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        new FileUploader2(new ArchivoDAO());
      }
    });
  }
}
