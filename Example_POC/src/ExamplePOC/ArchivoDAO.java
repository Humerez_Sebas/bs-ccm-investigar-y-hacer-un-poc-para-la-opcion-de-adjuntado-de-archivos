package ExamplePOC;

import java.awt.Desktop;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ArchivoDAO implements DAO<Archivo> {


  @Override
  public List<Archivo> getAll() {
    return null;
  }

  @Override
  public Archivo getById(int id) throws SQLException, IOException {
    ConexionMySQL con = new ConexionMySQL();
    byte[] b = null;
    String extension = "";

    try (Connection connection = con.getConnection();
        PreparedStatement ps = connection.prepareStatement(
            "SELECT doc, extension FROM image WHERE id = ?;")
    ) {
      ps.setInt(1, id);

      try (ResultSet rs = ps.executeQuery()) {
        if (rs.next()) {
          b = rs.getBytes("doc");
          extension = rs.getString("extension");
        }
      }
    } catch (SQLException e) {
      throw new SQLException("Error al recuperar el archivo desde la base de datos", e);
    }

    if (b != null) {
      File tempFile = File.createTempFile("tempFile", "." + extension);
      try (InputStream bos = new ByteArrayInputStream(b);
          FileOutputStream fos = new FileOutputStream(tempFile)) {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = bos.read(buffer)) != -1) {
          fos.write(buffer, 0, bytesRead);
        }
      }
      if (Desktop.isDesktopSupported()) {
        Desktop.getDesktop().open(tempFile);
      }
    }

    return new Archivo(id, b);
  }

  @Override
  public void update(Archivo archivo) {

  }

  @Override
  public void delete(Archivo archivo) {

  }

  @Override
  public void add(Archivo archivo) {
    ConexionMySQL con = new ConexionMySQL();
    String sql = "INSERT INTO image (doc, extension) VALUES(?, ?);";
    PreparedStatement ps = null;
    try {
      ps = con.getConnection().prepareStatement(sql);
      ps.setBytes(1, archivo.getArchivo());
      ps.setString(2, archivo.getExtension());
      ps.executeUpdate();
    } catch (SQLException ex) {
      System.out.println(ex.getMessage());
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
    } finally {
      try {
        if (ps != null) {
          ps.close();
        }
        con.closeConnection();
      } catch (Exception ex) {
        System.out.println(ex.getMessage());
      }
    }
  }

}